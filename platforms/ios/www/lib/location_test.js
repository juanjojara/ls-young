var serviceInstance;

var placesTypes = "airport|amusement_park|aquarium|art_gallery|bakery|bus_station|cafe|campground|church|city_hall|embassy|food|grocery_or_supermarket|gym|health|hindu_temple|library|local_government_office|lodging|mosque|movie_theater|museum|park|place_of_worship|post_office|restaurant|school|shopping_mall|spa|stadium|subway_station|synagogue|train_station|university|zoo";

var app = {
    //Function for creating a card ONCE
    shareLocation: function() {
        window.navigator.geolocation.getCurrentPosition(function(position) {
            console.log('SHARE LOCATION');
            if(navigator.connection.type == Connection.NONE){
                window.navigator.notification.confirm(
                    globalizationApp.getLanguageValue("dlgSaveCardNoInternetMsg"), 
                    function(confirmButton){
                        if (confirmButton == 1){
                            geolocFunctions.prepareCard(position.coords.latitude, position.coords.longitude);
                        }
                    }, 
                    globalizationApp.getLanguageValue("dlgSaveCardNoInternetTitle"), 
                    [globalizationApp.getLanguageValue("yes"),globalizationApp.getLanguageValue("dlgConfirmCardMaybe")]
                );
            }else{
                geolocFunctions.reverseGeolocation(position.coords.latitude, position.coords.longitude);
            }            
        });
    },

    finishBackgroundGeoLocation: function() {
        if (!(serviceInstance === undefined)) {
            console.log("*** PROCESS FINISHED ***");
            serviceInstance.finish();
        }
    },

    stopBackgroundGeoLocation: function() {
        if (!(serviceInstance === undefined)) {
            console.log('Service instance Not UNDEFINED');
            serviceInstance.stop();
        }else{
            console.log('Service instance Is UNDEFINED');
        }
    },

    resumeBackgroundGeoLocation: function() {
        if (!(serviceInstance === undefined)) {
            serviceInstance.start();
        }
    },

    configureBackgroundGeoLocation: function() {
        if (!(serviceInstance === undefined)) {
            serviceInstance.start();
            return true;
        }
        // Your app must execute AT LEAST ONE call for the current position via standard Cordova geolocation,
        //  in order to prompt the user for Location permission.
        window.navigator.geolocation.getCurrentPosition(function(position) {
            console.log('Location from Phonegap JJJ');
            console.log('[js] BackgroundGeoLocation config:  ' + position.coords.latitude + ',' + position.coords.longitude);

            if (localStorage.getItem("tkn") === null) {
                console.log("No TOKEN");
            }else{
                console.log("Yes TOKEN: " + localStorage.getItem("tkn"));
            }
            if (localStorage.getItem("location_setting") === null) {
                console.log("No LOC SET");
            }else{
                console.log("Yes LOC SET: " + localStorage.getItem("location_setting"));
            }
            if (localStorage.getItem("sharing_setting") === null) {
                console.log("No SHA SET");
            }else{
                console.log("Yes SHA SET: " + localStorage.getItem("sharing_setting"));
            }            
        });

        var bgGeo = window.plugins.backgroundGeoLocation;
        serviceInstance = bgGeo;

        /**
        * This would be your own callback for Ajax-requests after POSTing background geolocation to your server.
        */
        var yourAjaxCallback = function(response) {
            ////
            // IMPORTANT:  You must execute the #finish method here to inform the native plugin that you're finished,
            //  and the background-task may be completed.  You must do this regardless if your HTTP request is successful or not.
            // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
            //
            //
            console.log("*** PROCESS FINISHED ***");
            bgGeo.finish();
        };

        /**
        * This callback will be executed every time a geolocation is recorded in the background.
        */
        var callbackFn = function(position) {
            geolocFunctions.prepareCard(position.latitude, position.longitude);

            //yourAjaxCallback.call(this);
        };

        var failureFn = function(error) {
            console.log('BackgroundGeoLocation error');
        };
        
        // BackgroundGeoLocation is highly configurable.
        var gpsAccuracy = 0;
        if (device.platform == "Android"){
            gpsAccuracy = 100;
        }
        bgGeo.configure(callbackFn, failureFn, {
            url: 'https://ls-gae-api.appspot.com/_ah/api/card/v2/create_card', // <-- only required for Android; ios allows javascript callbacks for your http
            headers: {
                'Authorization': localStorage.getItem("tkn")
            },
            params: {
                'LocationSetting':localStorage.getItem("location_setting"),
                'SharingSetting':localStorage.getItem("sharing_setting"),
                'UserId':localStorage.getItem("user_id")
            },
            desiredAccuracy: gpsAccuracy,
            stationaryRadius: 50,
            distanceFilter: 50,
            notificationTitle: 'Lifeshare', // <-- android only, customize the title of the notification
            notificationText: globalizationApp.getLanguageValue("serviceNotificationText"), // <-- android only, customize the text of the notification
            activityType: 'Other',
            stopOnTerminate: false,
            debug: false, // <-- enable this hear sounds for background-geolocation life-cycle.
            locationTimeout: 600
        });

        // Turn ON the background-geolocation system.  The user will be tracked whenever they suspend the app.
        bgGeo.start();

        // If you wish to turn OFF background-tracking, call the #stop method.
        // bgGeo.stop()
    }
};

var geolocFunctions = {
    prepareCard: function(lat, lng){
        if(navigator.connection.type == Connection.NONE){
            var confirmationDlg = false;
            if (localStorage.getItem("sharing_setting") != 'automatic')
                confirmationDlg = true;
            //Save coords in DB for creating the card later
            var cardId = dbFunctions.getCardId();
            var cardDB = { 
                'id': cardId,
                'ts': Date.now(),
                'info': '',
                'location': '',
                'latitude': lat,
                'longitude': lng,
                'sharing_level': localStorage.getItem("sharing_setting"), 
                'location_level': localStorage.getItem("location_setting"),
                'confirm': confirmationDlg
            };
            dbFunctions.addCard(cardDB, "pending_geo", function(){
                if (localStorage.getItem("sharing_setting") != 'button'){
                    app.finishBackgroundGeoLocation();
                }
            }, function(err){
                console.log("Error inserting in Pending GEO");
                console.log("DB Error: "+err.message + ". Code="+err.code);
                if (localStorage.getItem("sharing_setting") != 'button'){
                    app.finishBackgroundGeoLocation();
                }
            });
        }else{
            //Create the card to share
            geolocFunctions.reverseGeolocation(lat, lng);
        }
    },

    //Place Or City
    reverseGeolocation: function(lat, lng){
        var http = new XMLHttpRequest();
        var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&sensor=true";
        
        if (geolocFunctions.location_level(localStorage.getItem("location_setting")) <= 0){
            url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lat + "," + lng + "&radius=50&types=" + placesTypes + "&key=AIzaSyAcMdSWYY56SKeMBIFtCHWnXXNfmn5tnj8";
            //url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lat + "," + lng + "&rankby=distance&types=" + placesTypes + "&key=AIzaSyAcMdSWYY56SKeMBIFtCHWnXXNfmn5tnj8";
        }

        http.onreadystatechange = function(){
            if (http.readyState == 4 && http.status == 200){
                var timeCard = Date.now();
                var curLocation = "";
                var curInfo = "";
                if (geolocFunctions.location_level(localStorage.getItem("location_setting")) <= 0){
                    curLocation = geolocFunctions.getPlace(this.responseText, localStorage.getItem("location_setting"), lat, lng);
                    curInfo = globalizationApp.getLanguageValue("cardInfoIsNear");
                }else{
                    curLocation = geolocFunctions.prepareLocation(this.responseText, localStorage.getItem("location_setting"));
                    curInfo = geolocFunctions.prepareInfo(timeCard);
                }
                var userShare_setting = localStorage.getItem("sharing_setting");
                if (userShare_setting == 'button'){
                    window.navigator.notification.confirm(
                        globalizationApp.getLanguageValue("dlgConfirmCardMsg") + curInfo + ' ' + curLocation, 
                        function(confirmButton){
                            if (confirmButton == 1){
                                geolocFunctions.share(null, curInfo, curLocation, lat, lng, timeCard, function(){
                                    var cardId = dbFunctions.getCardId();
                                    var cardDB = {
                                        'id': cardId,
                                        'ts': timeCard,
                                        'info': curInfo,
                                        'location': curLocation,
                                        'latitude': lat,
                                        'longitude': lng,
                                        'sharing_level': userShare_setting, 
                                        'location_level': localStorage.getItem("location_setting"),
                                        'confirm': false
                                    };
                                    dbFunctions.addCard(cardDB, "shared_cards", dbManagement.updateLastSharedList, function(err){
                                        console.log("Error inserting in shared cards during a button sharing");
                                        console.log("DB Error: "+err.message + ". Code="+err.code);
                                    });
                                }, function(){
                                    console.log("Error while doing a BUTTON sharing");
                                });
                            }
                        }, 
                        globalizationApp.getLanguageValue("dlgConfirmCardTitle"), 
                        [globalizationApp.getLanguageValue("dlgConfirmCardYes"),globalizationApp.getLanguageValue("dlgConfirmCardMaybe")]);
                }else{
                    //Check if the data is NEW to avoid creating redundant cards
                    var lastLocation = localStorage.getItem("lastLocation");
                    var lastInfo = localStorage.getItem("lastInfo");
                    if (!(lastLocation === null) && !(lastInfo === null)) {
                        if ((lastLocation == curLocation) && (lastInfo == curInfo)){
                            console.log('Redundant Card: Skipping creation');
                            dbFunctions.checkInternetPendings();
                            app.finishBackgroundGeoLocation();
                            return true;
                        }
                    }

                    //Define base card
                    var cardId = dbFunctions.getCardId();
                    var cardDB = {
                        'id': cardId,
                        'ts': timeCard,
                        'info': curInfo,
                        'location': curLocation,
                        'latitude': lat,
                        'longitude': lng,
                        'sharing_level': userShare_setting, 
                        'location_level': localStorage.getItem("location_setting"),
                        'confirm': false
                    };

                    //If the sharing needs confirmation then we send a notification instead
                    if (userShare_setting == "confirm"){
                        //Save card in DB for confirmation later
                        cardDB.confirm = true;
                        dbFunctions.addCard(cardDB, "pending_confirm", function(){
                            var jsonObjNot = {
                                notInfo: curInfo, 
                                notLoc: curLocation, 
                                notLat: lat, 
                                notLon: lng, 
                                notCardId: cardId,
                                notTs: timeCard
                            }
                            dbFunctions.sendNotification(dbFunctions.getNotifiactionId(), globalizationApp.getLanguageValue("serviceNotificationTapShareTitle"), globalizationApp.getLanguageValue("serviceNotificationTapShareText") + curInfo + ' ' + curLocation, jsonObjNot);
        
                            localStorage.setItem("lastLocation", curLocation);
                            localStorage.setItem("lastInfo", curInfo);

                            app.finishBackgroundGeoLocation();
                        }, function(err){
                            console.log("Error inserting in pending cards during a confirm sharing");
                            console.log("DB Error: "+err.message + ". Code="+err.code);
                            app.finishBackgroundGeoLocation();
                        });
                    }else{
                        //Share the card if it does not need user confirmation
                        if(navigator.connection.type == Connection.NONE){
                            //Save card in DB for to share when Internet is available
                            dbFunctions.addCard(cardDB, "pending_internet", function(){
                                localStorage.setItem("lastLocation", curLocation);
                                localStorage.setItem("lastInfo", curInfo);
                                app.finishBackgroundGeoLocation();
                            }, function(err){
                                console.log("Error inserting in pending internet during an automatic sharing");
                                console.log("DB Error: "+err.message + ". Code="+err.code);
                                app.finishBackgroundGeoLocation();
                            });
                        }else{
                            if (!(localStorage.getItem("pendingInternet") === null)) {
                                if (localStorage.getItem("pendingInternet") == "true"){
                                    var pendingInt = 'internet';
                                    var jsonObjNot = {
                                        notInfo: pendingInt, 
                                        notLoc: curLocation, 
                                        notLat: lat, 
                                        notLon: lng, 
                                        notCardId: cardId,
                                        notTs: timeCard
                                    }
                                    dbFunctions.sendNotification(dbFunctions.getNotifiactionId(), globalizationApp.getLanguageValue("serviceNotificationShareInternetTitle"), globalizationApp.getLanguageValue("serviceNotificationShareInternetText"), jsonObjNot);
                                }
                            }                            

                            geolocFunctions.share(null, curInfo, curLocation, lat, lng, timeCard, function(){
                                localStorage.setItem("lastLocation", curLocation);
                                localStorage.setItem("lastInfo", curInfo);
                                dbFunctions.addCard(cardDB, "shared_cards", app.finishBackgroundGeoLocation, function(err){
                                    console.log("Error inserting in shared cards during an automatic sharing");
                                    console.log("DB Error: "+err.message + ". Code="+err.code);
                                    app.finishBackgroundGeoLocation();
                                });
                            }, app.finishBackgroundGeoLocation);
                        }
                    }
                }
            }
        }
        http.open("GET", url, true);
        //Send the proper header information along with the request
        http.setRequestHeader('Content-Type', 'application/json');
        http.send();
    },

    //Recovering a card missing geo info when internet is available 
    reverseGeolocationRecovery: function(lat, lng, recTs, userLoc_setting, completeCB, redundantCB){
        var http = new XMLHttpRequest();
        var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&sensor=true";

        if (geolocFunctions.location_level(userLoc_setting) <= 0){
            url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + lat + "," + lng + "&radius=50&types=" + placesTypes + "&key=AIzaSyAcMdSWYY56SKeMBIFtCHWnXXNfmn5tnj8";
        }
        http.onreadystatechange = function(){
            if (http.readyState == 4 && http.status == 200){
                var curLocation = "";
                var curInfo = "";
                if (geolocFunctions.location_level(userLoc_setting) <= 0){
                    curLocation = geolocFunctions.getPlace(this.responseText, userLoc_setting, lat, lng);
                    curInfo = globalizationApp.getLanguageValue("cardInfoIsNear");
                }else{
                    curLocation = geolocFunctions.prepareLocation(this.responseText, userLoc_setting);
                    curInfo = geolocFunctions.prepareInfo(recTs);
                }

                //Check if the data is NEW to avoid creating redundant cards
                var lastLocation = localStorage.getItem("lastLocation");
                var lastInfo = localStorage.getItem("lastInfo");
                if (!(lastLocation === null) && !(lastInfo === null)) {
                    if ((lastLocation == curLocation) && (lastInfo == curInfo)){
                        console.log('Reverse Geo - Redundant Card: Skipping creation');
                        redundantCB();
                        return true;
                    }
                }
                localStorage.setItem("lastLocation", curLocation);
                localStorage.setItem("lastInfo", curInfo);

                completeCB(curInfo, curLocation);
            }
        }
        http.open("GET", url, true);
        //Send the proper header information along with the request
        http.setRequestHeader('Content-Type', 'application/json');
        http.send();
    },

    getPlace: function(reverseGeoResponse, userLoc_setting, lat, lng){
        var places_list = JSON.parse(reverseGeoResponse);

        var curLocation = "unavailable";
        if (typeof(places_list.results) != "undefined"){
            console.log('GET PLACE OK');
            var curPlace = places_list.results[0];
            curLocation = curPlace.name + " (" + globalizationApp.getLanguageValue("pt_" + curPlace.types[0]) + ")";
            console.log(curLocation);

            //GET PHOTO OF THE PLACE
            //https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=CnRtAAAATLZNl354RwP_9UKbQ_5Psy40texXePv4oAlgP4qNEkdIrkyse7rPXYGd9D_Uj1rVsQdWT4oRz4QrYAJNpFX7rzqqMlZw2h2E2y5IKMUZ7ouD_SlcHxYq1yL4KbKUv3qtWgTK0A6QbGh87GB3sscrHRIQiG2RrmU_jF4tENr9wGS_YxoUSSDrYjWmrNfeEHSGSc3FyhNLlBU&key=AIzaSyAcMdSWYY56SKeMBIFtCHWnXXNfmn5tnj8
            if (typeof(curPlace.photos) != "undefined"){
                console.log('GET PLACE PHOTO');
                /*  WE DO NOT DO THIS NOW, WE FIGURE OUT LATER HOW TO USE THE SPECIFIC PHOTO FOR A PLACE
                var photoHttp = new XMLHttpRequest();
                var photoUrl = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=" + 1024 + "&photoreference=" + curPlace.photos[0].photo_reference + "&key=AIzaSyAcMdSWYY56SKeMBIFtCHWnXXNfmn5tnj8";
                //var photoUrl = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=CnRtAAAATLZNl354RwP_9UKbQ_5Psy40texXePv4oAlgP4qNEkdIrkyse7rPXYGd9D_Uj1rVsQdWT4oRz4QrYAJNpFX7rzqqMlZw2h2E2y5IKMUZ7ouD_SlcHxYq1yL4KbKUv3qtWgTK0A6QbGh87GB3sscrHRIQiG2RrmU_jF4tENr9wGS_YxoUSSDrYjWmrNfeEHSGSc3FyhNLlBU&key=AIzaSyAcMdSWYY56SKeMBIFtCHWnXXNfmn5tnj8";
                photoHttp.onreadystatechange = function(){
                    if (photoHttp.readyState == 4 && photoHttp.status == 200){
                        console.log('URL OF PLACE PHOTO');
                        console.log(photoHttp.getAllResponseHeaders());
                        console.log('URL OF PLACE PHOTO');
                    }
                }
                photoHttp.open("GET", photoUrl, true);
                //Send the proper header information along with the request
                photoHttp.send();*/
            }else{
                console.log('PLACE WITHOUT PHOTO');
            }
            return curLocation;
        }else{
            console.log('GET PLACE FAILED. RETRIEVING CITY INSTEAD');
            var httpCity = new XMLHttpRequest();
            var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&sensor=true";
            
            httpCity.onreadystatechange = function(){
                if (httpCity.readyState == 4 && httpCity.status == 200){
                    curLocation = geolocFunctions.prepareLocation(this.responseText, userLoc_setting);
                    return curLocation;
                }
            }
            httpCity.open("GET", url, true);
            //Send the proper header information along with the request
            httpCity.setRequestHeader('Content-Type', 'application/json');
            httpCity.send();        
        }
    }, 

    share: function(shareCardId, shareInfo, shareLoc, shareLat, shareLon, shareTs, successCB, failedCB){
        console.log('Create card');
        console.log('INFO: ' + shareInfo);
        console.log('LOCATION: ' + shareLoc);
        console.log('LATITUDE: ' + shareLat);
        console.log('LONGITUDE: ' + shareLon);
        console.log('TS: ' + shareTs);

        var http = new XMLHttpRequest();
        var url = "https://ls-gae-api.appspot.com/_ah/api/card/v2/create_card";
        var params = { 
            'info': shareInfo,
            'lat': shareLat,
            'lon': shareLon,
            'location': shareLoc,
            'timestamp': shareTs
        };
        http.onreadystatechange = function(){
            if (http.readyState == 4) {
                if (http.status == 200 || http.status == 204) {
                    console.log("SHARE CARD successful");
                    successCB(shareCardId);
                }else{
                    console.log("SHARE CARD failed");
                    failedCB();
                }
            }
        }
        http.open("POST", url, true);

        //Send the proper header information along with the request
        http.setRequestHeader('Content-Type', 'application/json');
        http.setRequestHeader('Authorization',localStorage.getItem("tkn"));

        http.send(JSON.stringify(params));
    },

    prepareLocation: function(reverseGeoResponse, userloc_setting){
        var addr = (JSON.parse(reverseGeoResponse)).results[0];
        var addr_comp = addr.address_components;
        var curCity;
        var curCityBkp;
        var curRegion;
        var curCountry;
        for (i=0; i<addr_comp.length;i++){
            if ( addr_comp[i].types[0] == "administrative_area_level_1" || addr_comp[i].types[0] == "administrative_area_level_2" ) {
                curRegion = addr_comp[i].long_name;
            } else if ( addr_comp[i].types[0] == "country") {
                curCountry = addr_comp[i].long_name;
            } else if ( addr_comp[i].types[0] == "locality") {
                curCity = addr_comp[i].long_name;
            } else if ( addr_comp[i].types[0] == "administrative_area_level_3" ) {
                curCityBkp = addr_comp[i].long_name;
            }
        }
        if ((typeof curCity === "undefined") || (curCity === null) || (curCity == "")){
            curCity = curCityBkp;
        }

        var curLocation = "unavailable";
        if (!(typeof curCity === "undefined") && (geolocFunctions.location_level(userloc_setting) <= 1)){
            curLocation = curCity;
            if (!(typeof curRegion === "undefined")){
                curLocation = curLocation + ', ' + curRegion;
            }
            if (!(typeof curCountry === "undefined")){
                curLocation = curLocation + ', ' + curCountry;
            }
        }else if (!(typeof curRegion === "undefined") && (geolocFunctions.location_level(userloc_setting) <= 2)){
            curLocation = curRegion;
            if (!(typeof curCountry === "undefined")){
                curLocation = curLocation + ', ' + curCountry;
            }
        }else if (!(typeof curCountry === "undefined") && (geolocFunctions.location_level(userloc_setting) <= 3)){
            curLocation = curCountry;
        }
        return curLocation;      
    }, 

    prepareInfo: function(curTs){
        var currentdate = new Date(curTs);

        var Info = globalizationApp.getLanguageValue("cardInfoIsAt");
        if (currentdate.getHours<=6){
            Info = globalizationApp.getLanguageValue("cardInfoSleeping");
        } else if (currentdate.getHours>= 12 && currentdate.getHours<=14){
            Info = globalizationApp.getLanguageValue("cardInfoLunch");
        }
        return Info;            
    },

    location_level: function(loc_level){
        var ret_level;
        switch(loc_level) {
            case "place":
                ret_level=0;
                break;
            case "city":
                ret_level=1;
                break;
            case "region":
                ret_level=2;
                break;
            case "country":
                ret_level=3;
                break;
            default:
                ret_level=4;
        }
        return ret_level;
    }
};

var dbFunctions = {
    addCard: function(card, table, successCB, errorCB){
        var user_id = localStorage.getItem("user_id");
        var db = null;
        if (device.platform == "Android"){
            db = window.openDatabase("dbLifeshare", "1", "Lifeshare Cards", 1*1024*512);
        }else{
            db = window.openDatabase("dbLifeshare", "1.0", "Lifeshare Cards", 1*1024*1024);                
        }
        //var db = window.sqlitePlugin.openDatabase({name: "dbLifeshare.db"});
        if (errorCB === null){
            console.log("Callback is null");
            errorCB = dbErrorHandler;
        }
        db.transaction(insertCard, errorCB);

        function insertCard(tx){
            console.log("INSERT: " + table + " | " + card.info + " - " + card.location + " - " + card.latitude + " - " + card.longitude + " - " + card.confirm);
            var insertQuery = "INSERT INTO " + table + " (id, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm) VALUES (?,?,?,?,?,?,?,?,?,?)";
            
            if (successCB === null){
                console.log("Success callback is null");
                successCB = dbSuccess;
            }
            tx.executeSql(insertQuery,[card.id, card.ts, card.info, card.location, card.latitude, card.longitude, card.sharing_level, card.location_level, user_id, card.confirm], successCB, errorCB);
        }

        function dbErrorHandler(err){
            console.log("Insert Error");
            console.log("DB Error: "+err.message + ". Code="+err.code);
        }

        function dbSuccess(){
            console.log("Insert successful");
        }
    },

    checkInternetPendings: function(){
        var user_id = localStorage.getItem("user_id");
        var db = null;
        if (device.platform == "Android"){
            db = window.openDatabase("dbLifeshare", "1", "Lifeshare Cards", 1*1024*512);
        }else{
            db = window.openDatabase("dbLifeshare", "1.0", "Lifeshare Cards", 1*1024*1024);
        }
        //var db = window.sqlitePlugin.openDatabase({name: "dbLifeshare.db"});
        db.transaction(checkPendings, dbFunctions.dbErrorHandler);

        function checkPendings(tx){
            console.log("Check if there are internet pending cards");

            var checkQuery = "SELECT count(id) countPendings FROM pending_geo WHERE user_id = ?";
            tx.executeSql(checkQuery,[user_id], sendReminder, dbFunctions.dbErrorHandler);
        }

        function sendReminder(tx,results){
            var resCount = results.rows.item(0).countPendings;
            console.log("RC: " + resCount);
            if (resCount > 0){
                localStorage.setItem("pendingInternet", "true");
            }else{
                localStorage.setItem("pendingInternet", "false");
            }
        }
    },

    getCardId: function(){
        var cardId;
        if (!(localStorage.getItem("cardId") === null)) {
            cardId = parseInt(localStorage.getItem("cardId"));
        }else{
            cardId = 1;
        }
        localStorage.setItem("cardId", +cardId + +1);
        return cardId;
    },

    getNotifiactionId: function(){
        var notificationId;
        if (!(localStorage.getItem("notificationId") === null)) {
            notificationId = parseInt(localStorage.getItem("notificationId"));
            if (notificationId >= 65)
                notificationId = 1;
        }else{
            notificationId = 1;
        }
        localStorage.setItem("notificationId", notificationId + 1);
        return notificationId;
    },

    getConfirmBadge: function(){
        var confirmBadge;
        if (!(localStorage.getItem("confirmBadge") === null)) {
            confirmBadge = parseInt(localStorage.getItem("confirmBadge"));
        }else{
            confirmBadge = 0;
        }
        localStorage.setItem("confirmBadge", confirmBadge + 1);
        return confirmBadge + 1;
    },

    sendNotification: function(idNot, titleNot, messageNot, jsonNot){
        console.log("PRE FIRING FUNC NOT");
        //Send the notification
        window.plugin.notification.local.schedule({
            id: idNot,
            title: titleNot,
            text: messageNot,
            badge: dbFunctions.getConfirmBadge(),
            data: JSON.stringify(jsonNot) 
        });
        //https://github.com/Juanjojara/cordova-plugin-local-notifications.git
        console.log("POST FIRING FUNC NOT");
    },

    dbErrorHandler: function(err){
        console.log("Insert Error");
        console.log("DB Error: "+err.message + ". Code="+err.code);
    },

    dbSuccess: function(){
        console.log("Insert successful");
    }
};
