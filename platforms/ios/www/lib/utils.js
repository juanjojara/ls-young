var utils = utils || {};

/*utils.getItem = function(key){
    if (device.platform == "Android"){
        plugins.appPreferences.fetch (function(value){
        	console.log("CCC");
            return value;
        }, function(){
            console.log("Error getting " + key + " from storage");
            console.log("DDD");
            return "";
        }, key);
    } else {
    	if (!(localStorage.getItem(key) === null)) {
	        return localStorage.getItem(key);
	    }else{
	    	return "";
	    }
    }
}*/

utils.getItem = function(key, successCB, failCB){
        plugins.appPreferences.fetch (function(value){
        	successCB(value);
        }, failCB, key);
}

utils.setItem = function(key, value){
    plugins.appPreferences.store (function(){
        console.log(value + " stored in " + key);
    }, function(){
        console.log("Error setting " + value + " in key " + key);
    }, key, value);
}