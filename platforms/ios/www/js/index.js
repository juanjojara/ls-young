var googleapi = {
    authorize: function(options) {
        var deferred = $.Deferred();

        //Build the OAuth consent page URL
        var authUrl = 'https://accounts.google.com/o/oauth2/auth?' + $.param({
            client_id: options.client_id,
            redirect_uri: options.redirect_uri,
            response_type: 'code',
            scope: options.scope
        });

        //Open the OAuth consent page in the InAppBrowser
        var authWindow = window.open(authUrl, '_blank', 'location=no,toolbar=no');

        //The recommendation is to use the redirect_uri "urn:ietf:wg:oauth:2.0:oob"
        //which sets the authorization code in the browser's title. However, we can't
        //access the title of the InAppBrowser.
        //
        //Instead, we pass a bogus redirect_uri of "http://localhost", which means the
        //authorization code will get set in the url. We can access the url in the
        //loadstart and loadstop events. So if we bind the loadstart event, we can
        //find the authorization code and close the InAppBrowser after the user
        //has granted us access to their data.
        $(authWindow).on('loadstart', function(e) {
            var url = e.originalEvent.url;
            var code = /\?code=(.+)$/.exec(url);
            var error = /\?error=(.+)$/.exec(url);

            if (code || error) {
                //Always close the browser when match is found
                authWindow.close();
            }

            if (code) {
                var code_v1=code[1].split("&");
                //Exchange the authorization code for an access token
                $.post('https://accounts.google.com/o/oauth2/token', {
                    code: code_v1[0],
                    client_id: options.client_id,
                    client_secret: options.client_secret,
                    redirect_uri: options.redirect_uri,
                    grant_type: 'authorization_code'
                }).done(function(data) {
                    deferred.resolve(data);
                }).fail(function(response) {
                    deferred.reject(response.responseJSON);
                });
            } else if (error) {
                //The user denied access to the app
                deferred.reject({
                    error: error[1]
                });
            }
        });

        return deferred.promise();
    }
};

$('#btnGoogle').hide();
$('#btnFb').hide();
$(document).on('deviceready', function() {
    if (!(localStorage.getItem("tkn") === null)) {
        window.location="main.html";
    }else{
        //Globalization init
        globalizationApp.globalizationInit(function(){
            console.log("Globalization Init Index");
        });
    }
    
    var $loginGoogle = $('#btnGoogle');
    var $loginFb = $('#btnFb');
    openFB.init({appId: '783907758296055'}); // Defaults to sessionStorage for storing the Facebook token

    $loginGoogle.on('click', function() {
        googleapi.authorize({
            client_id: '1063218928873-ecr9rqr477r4tfisdsmqa61a2dhhu2dr.apps.googleusercontent.com',
            client_secret: '9V-ntBjrlVI2noicQkZgMiFf',
            redirect_uri: 'http://localhost',
            scope: 'openid profile email'
        }).done(function(data) {
            ls.social_login("GOOGLE", data.access_token);
        }).fail(function(data) {
            $("#loginPopup p").html(globalizationApp.getLanguageValue("popupErrorLogin") + data.error);
            setTimeout(function () {
                $('#loginPopup').popup('close');
                $("#loginPopup p").html(globalizationApp.getLanguageValue("popupConnecting"));
            }, 3000);
        });
    });

    $loginFb.on('click', function() {
        openFB.login(
            function(response) {
                if(response.authResponse) {
                    ls.social_login("FB", response.authResponse.token);
                } else {
                    $("#loginPopup p").html(globalizationApp.getLanguageValue("popupErrorLogin") + response.error_description);
                    setTimeout(function () {
                        $('#loginPopup').popup('close');
                        $("#loginPopup p").html(globalizationApp.getLanguageValue("popupConnecting"));
                    }, 3000);
                }
            }, {scope: 'public_profile,email'});
    });
});

var ls = ls || {};
ls.social_login = function(platform, token){
    console.log("login " + platform + " - " + token);
    gapi.client.user.oauth_login({"platform":platform,"token": token}).execute(function(resp) {
        if (!resp.code) {
            sessionStorage.setItem('id', resp.id);
            sessionStorage.setItem('token', resp.token);
            localStorage.setItem("tkn", resp.token);
            localStorage.setItem("user_id", resp.id);
            $('#loginPopup').popup('close');
            window.location="main.html";
        }else{
            $("#loginPopup p").html(globalizationApp.getLanguageValue("popupErrorLogin") + resp.message);
            setTimeout(function () {
                $('#loginPopup').popup('close');
                $("#loginPopup p").html(globalizationApp.getLanguageValue("popupConnecting"));
            }, 3000);
        }
        
    });
}

function init() {
    var apiRoot = "https://ls-gae-api.appspot.com/_ah/api";
    var apisToLoad;

    var callback = function() {
        console.log("APIs: " + apisToLoad);
        if (--apisToLoad == 0) {
            $('#btnGoogle').show();
            $('#btnFb').show();
        }
    }
    
    apisToLoad = 1; // must match number of calls to gapi.client.load()
    //gapi.client.load('card', 'v2', callback, apiRoot);
    gapi.client.load('user', 'v2', callback, apiRoot);
    
}