
$(document).on('deviceready', function() {
    selectedFId =  localStorage.getItem("followerIdVC");
    selectedFName =  localStorage.getItem("followerNameVC");
    console.log("Follower: " + selectedFId + " - " + selectedFName);

    video_layout = JSON.parse('{}');
    var screenWidth = $(window).width();
    //VIDEO EJS
    console.log("W: " + $(window).width() + " - H: " + $(window).height());
    video_layout.myVideoTop = $(window).height()*0.8-70;
    video_layout.myVideoLeft = $(window).width()*0.8;
    video_layout.callerVideoTop = 80;
    video_layout.callerVideoLeft = 0;
    video_layout.bottomBarTop = $(window).height() - 70;
    video_layout.callingUserTop = $(window).height()*0.3;
    video_layout.callingUserLeft = $(window).width()*0.3;
    video_layout.followerName = selectedFName;
    if (screenWidth <= 350){
        video_layout.fontSize = 15;
        video_layout.buttonWidth = screenWidth - 20;
        video_layout.iconWidth = screenWidth*0.1;
        video_layout.iconHeight = screenWidth*0.05;
        video_layout.labelLeft = screenWidth*0.15;
        video_layout.labelWidth = screenWidth*0.85;
    }else{
        video_layout.fontSize = 30;
        video_layout.buttonWidth = 350;
        video_layout.iconWidth = 65;
        video_layout.iconHeight = 35;
        video_layout.labelLeft = 85;
        video_layout.labelWidth = screenWidth*0.85;
    }

    var result = new EJS({url: 'ejs/videoMobile.ejs'}).render(video_layout);
    $('#video').html(result);
    //Resize
    $('.fullWidth').css('width', $(window).width() + 'px');


    $("#picUserCalled").attr("src", localStorage.getItem("followerPicVC"));

    globalizationApp.globalizationInit(function(){
        $('#userCallLabl').html(globalizationApp.getLanguageValue("callingUserMessage") + "...");
        $('#videoCallLabel').html(globalizationApp.getLanguageValue("videoCallFooterLabel") + selectedFName);
        preCall();

        $('#btnEndVideoCall').on('click', function () {
            EndPreCall();
        });
    });
    window.plugins.insomnia.keepAwake();
});

function VideoCall(){
    var xmlhttp = new XMLHttpRequest();
    var sessionURL = "https://opentokrtc.com/" + localStorage.getItem("user_id") + "_" + selectedFId + ".json";
    console.log("Follower URL: " + sessionURL);
    xmlhttp.open("GET", sessionURL, false);
    xmlhttp.send();
    var data = JSON.parse( xmlhttp.response );

    sessionVC = TB.initSession(data.apiKey, data.sid);
    var myVideoWidth = $(window).width()*0.18;
    var myVideoHeight = $(window).height()*0.18;
    publisherVC = TB.initPublisher(data.apiKey,'myPublisherDiv', {width:myVideoWidth, height:myVideoHeight} );
    $('#btnEndVideoCall').off();
    $('#btnEndVideoCall').on('click', function () {
        sessionVC.signal({
            type: "status",
            data: "ciao" + localStorage.getItem("followerIdVC")
        },function(error){
            console.log("SIGNAL CALLBACK");
            if (error) {
              console.log("signal error: " + error.message);
            } else {
              console.log("signal sent");
            }
        });
        EndCall();
    });
    sessionVC.on({
        'streamCreated': function( event ){
            localStorage.removeItem("waitingVideo");
            $("#waitingAnswer").hide();
            console.log(selectedFName + " connected to the session and STREAM STARTED");
            var videoWidth = $(window).width();
            var videoHeight = $(window).height()-150;
            subscriberVC = sessionVC.subscribe( event.stream, 'otherPublisherDiv', {width:videoWidth, height:videoHeight} );
            
            //var subscriber = sessionVC.subscribe( event.stream, 'otherPublisherDiv');
        },
        'streamDestroyed': function( event ){
            console.log("STREAM DESTROYED");
            //EndCall(sessionVC);            
            localStorage.setItem("callEndedUnexpectedly", "true");
            setTimeout(callInterrupted, 10000);
        },
        'sessionDisconnected': function( event ){
            console.log("SESSION FINISHED");
            console.log("The session disconnected. " + event.reason);
            //window.location = "main.html";
            localStorage.setItem("callEndedUnexpectedly", "true");
            setTimeout(callInterrupted, 12000);
        },
        'connectionDestroyed': function( event ){
            console.log("SESSION ENDED IM THE OTHER END POINT");
            //EndCall(sessionVC);
            localStorage.setItem("callEndedUnexpectedly", "true");
            setTimeout(callInterrupted, 14000);
        },
        'signal': function( event ){
            console.log("Signal data: " + event.data);
            if (event.data == "ciao" + localStorage.getItem("user_id")){
                EndCall();
            }
        }
    });
    sessionVC.connect(data.token, function(){
        console.log("Waiting for:" + selectedFName + " to connect. " + selectedFId);
        console.log("Session ID: " + data.sid);
        sessionVC.publish( publisherVC );
        localStorage.setItem("waitingVideo", "true");
        setTimeout(function(){
            if(!(localStorage.getItem("waitingVideo") === null)){
                localStorage.removeItem("waitingVideo");
                EndCall();
            }
        }, 60000);
    });
}


function preCall(){
    var confirmationSignal = localStorage.getItem("user_id") + "_" + selectedFId;

    var xmlhttp = new XMLHttpRequest();
    var sessionURL = "https://opentokrtc.com/lsvideocall" + selectedFId + ".json";
    console.log("Follower URL: " + sessionURL);
    xmlhttp.open("GET", sessionURL, false);
    xmlhttp.send();
    var data = JSON.parse( xmlhttp.response );
    console.log("API KEY: " + data.apiKey);
    console.log("Session ID: " + data.sid);
    sessionDroid = TB.initSession(data.apiKey, data.sid);
    sessionDroid.off();
    sessionDroid.on({
        'connectionCreated': function( event ){
            console.log(selectedFName + " connected to the session");
            sessionDroid.signal({
                type: "status",
                data: localStorage.getItem("user_id")
              },
              function(error) {
                if (error) {
                  console.log("signal error: " + error.message);
                } else {
                  console.log("signal sent");
                }
              }
            );
        },
        'sessionDisconnected': function( event ){
            console.log("SESSION FINISHED");
        },
        'signal': function( event ){
            console.log("Signal data: " + event.data);
            if (event.data.indexOf("_") >= 0){
                if (event.data == confirmationSignal){
                    sessionDroid.off();
                    sessionDroid.disconnect();
                    VideoCall();
                }else{
                    sessionDroid.disconnect();
                    var userBusyMessage = globalizationApp.getLanguageValue("userBusyMessage");
                    navigator.notification.alert(
                        userBusyMessage,
                        null,
                        globalizationApp.getLanguageValue("userBusyTitle"),
                        globalizationApp.getLanguageValue("popupButtonDone")
                    );
                }
            }
        }
    });
    sessionDroid.connect(data.token, function(){
        console.log("Waiting for:" + selectedFName + " to connect. " + selectedFId);
    });
}

function EndCall(){
    sessionVC.off();
    sessionVC.disconnect();
    localStorage.removeItem("callEndedUnexpectedly");
    localStorage.removeItem("waitingVideo");
    window.plugins.insomnia.allowSleepAgain();
    window.location = "main.html";
}

function EndPreCall(){
    sessionDroid.off();
    sessionDroid.disconnect();
    window.plugins.insomnia.allowSleepAgain();
    window.location = "main.html";
}

function callInterrupted(){
    if(!(localStorage.getItem("callEndedUnexpectedly") === null)){
        EndCall();
    }
}