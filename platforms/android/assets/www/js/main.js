$(document).on("pagebeforecreate","#pagemain",function(event){
	//globalizationApp.globalizationInit();
});

$(document).on("pagecreate","#pagemain",function(event){
	$('#btnSharing').hide();
	$('#shakeLabels').hide();
	$('#btnRefreshList').hide();

	//Here globalization does not work because it was not initialized yet
	if (!(localStorage.getItem("share") === null)) {
		if (localStorage.getItem("share") == "true"){
			$('#btnSharing').html("Pause Sharing");
		}else{
			$('#btnSharing').html("Resume Sharing");
		}
	}else{
		$('#btnSharing').html("Pause Sharing");
	}

	//This does not execute inmediately so is OK to use globalization
	$('#btnSharing').on('click', function() {
		if (localStorage.getItem("share") == "true") {
			localStorage.setItem("share", "false");
			//$('#btnSharing').html("Resume Sharing");
			$('#btnSharing').html(globalizationApp.getLanguageValue("buttonResumeShare"))
			if (window.plugins.backgroundGeoLocation) {
        	    console.log("PAUSIMG BACKGROUND SERVICE");
    	        app.stopBackgroundGeoLocation();
	            console.log("BACKGROUND SERVICE PAUSED");
            }
		}else{
			localStorage.setItem("share", "true");
			//$('#btnSharing').html("Pause Sharing");
			$('#btnSharing').html(globalizationApp.getLanguageValue("buttonPauseShare"));
			if (window.plugins.backgroundGeoLocation) {
        	    console.log("RESUMING BACKGROUND SERVICE");
        	    app.configureBackgroundGeoLocation();
    	        //app.resumeBackgroundGeoLocation();
	            console.log("BACKGROUND SERVICE RESUMED");
            }
		}
	});

	$('#btnShareOnce').on('click', function() {
		console.log("BUTTON SHARING!!");
		userManagement.shareLocApp();
	});

	$('#liMain').on('click', function() {
		var tutorialMessage1 = globalizationApp.getLanguageValue("tutorial1");
		navigator.notification.alert(tutorialMessage1, null, globalizationApp.getLanguageValue("popupTitleTutorial1"), globalizationApp.getLanguageValue("popupButtonDone"));
	});
});

$(document).on("pagecreate","#pagefollowers",function(event){
	followerManagement.getMyFollowers();

	$(this).on("click", "#followersList a", function (e) {
		//get the id and username of the selected list item
		var selectedUser = $(this).closest("li").attr("id");
		var selectedName = $(this).closest("li").attr("user-name");
		var selectedPic = $(this).closest("li").attr("user-pic");

		localStorage.setItem("followerIdVC", selectedUser);
		localStorage.setItem("followerNameVC", selectedName);
		localStorage.setItem("followerPicVC", selectedPic);
		
		if (device.platform == "Android"){
			window.location = "lsvideoAndroid.html";
		} else {
			window.location = "lsvideoiOS.html";
		}
	});	

	$('#liFollowers').on('click', function() {
		var tutorialMessage1 = globalizationApp.getLanguageValue("tutorial2");
		navigator.notification.alert(tutorialMessage1, null, globalizationApp.getLanguageValue("popupTitleTutorial2"), globalizationApp.getLanguageValue("popupButtonDone"));
	});
});

$(document).on("pageshow", "#pagefollowers", function () {
	//START TUTORIAL FOR NEW USER
	if (!(localStorage.getItem("tutorialFollowers") === null)) {
		var tutorialMessage2 = globalizationApp.getLanguageValue("tutorial2");
		navigator.notification.alert(tutorialMessage2, null, globalizationApp.getLanguageValue("popupTitleTutorial2"), globalizationApp.getLanguageValue("popupButtonDone"));
    	localStorage.removeItem("tutorialFollowers");
	}
});

$(document).on("pagecreate","#pageaddfollowers",function(event){
	followerManagement.getNotMyFollowers();

	$(this).on("click", "#usersList a", function (e) {
		//prevent dialog transition 
		e.preventDefault();
		//get the data-chapter
		var sourceId = $(this).closest("li").attr("id");
		var sourceName = $(this).closest("li").attr("user-name");
		var sourcePic = $(this).closest("li").attr("user-pic");
		//set that in #dialogPage's data for later use
		$("#YesNoDialogPage").data("selectedUser", sourceId);
		$("#YesNoDialogPage").data("selectedName", sourceName);
		$("#YesNoDialogPage").data("action", "add");
		$("#YesNoDialogPage").data("selectedPic", sourcePic);
		//redirect to dialog
		$.mobile.changePage(this.href, {
		    role: "dialog"
		});
	});
});

$(document).on("pagecreate","#pagedeletefollowers",function(event){
	followerManagement.removeMyFollowers();

	$(this).on("click", "#followersDeleteList a", function (e) {
		//prevent dialog transition 
		e.preventDefault();
		//get the data-chapter
		var sourceId = $(this).closest("li").attr("id");
		var sourceName = $(this).closest("li").attr("user-name");
		var sourcePic = $(this).closest("li").attr("user-pic");
		//set that in #dialogPage's data for later use
		$("#YesNoDialogPage").data("selectedUser", sourceId);
		$("#YesNoDialogPage").data("selectedName", sourceName);
		$("#YesNoDialogPage").data("action", "delete");
		$("#YesNoDialogPage").data("selectedPic", sourcePic);
		//redirect to dialog
		$.mobile.changePage(this.href, {
		    role: "dialog"
		});
	});
});

$(document).on("pagebeforeshow", "#YesNoDialogPage", function () {
    //get the chapter Id from data
    var selectedUser=  $(this).data("selectedUser");
    var selectedName=  $(this).data("selectedName");
    var selectedAction=  $(this).data("action");
    var selectedPic=  $(this).data("selectedPic");

    //do anything you want with it.
    var dlgMsg = "";
    if (selectedAction == "add"){
    	dlgMsg = globalizationApp.getLanguageValue("dlgMsgAddFollower");
    }else{
    	dlgMsg = globalizationApp.getLanguageValue("dlgMsgRemoveFollower");
    }
    $(this).find('[data-role="content"] p')
        .html(dlgMsg + selectedName);
    $('#picUser').attr("src", selectedPic);

	$('#btnConfirm').on('click', function() {
		if (selectedAction == "add"){
			followerManagement.addFollower(selectedUser);
		}else{
			followerManagement.removeFollower(selectedUser);
		}
	});
});

$(document).on("pagebeforehide", "#YesNoDialogPage", function () {
	$('#btnConfirm').off();
});

$(document).on("pagebeforeshow", "#CallingDialogPage", function () {
    //get the user Id and Name from data
    var selectedUser=  $(this).data("selectedUser");
    var selectedName=  $(this).data("selectedName");
    var selectedPic=  $(this).data("selectedPic");

	var confirmationSignal = localStorage.getItem("user_id") + "_" + selectedUser;
	localStorage.setItem("followerIdVC", selectedUser);
	localStorage.setItem("followerNameVC", selectedName);
	localStorage.setItem("followerPicVC", selectedPic);

	var xmlhttp = new XMLHttpRequest();
    var sessionURL = "https://opentokrtc.com/lsvideocall" + selectedUser + ".json";
    console.log("Follower URL: " + sessionURL);
    xmlhttp.open("GET", sessionURL, false);
    xmlhttp.send();
    var data = JSON.parse( xmlhttp.response );
    var session = TB.initSession(data.apiKey, data.sid);
    session.off();
    session.on({
        'connectionCreated': function( event ){
            console.log(selectedName + " connected to the session");
            session.signal({
                type: "status",
                data: localStorage.getItem("user_id")
              },
              function(error) {
                if (error) {
                  console.log("signal error: " + error.message);
                } else {
                  console.log("signal sent");
                }
              }
            );
        },
        'sessionDisconnected': function( event ){
            console.log("SESSION FINISHED");
        },
        'signal': function( event ){
            console.log("Signal data: " + event.data);
            if (event.data.indexOf("_") >= 0){
				if (event.data == confirmationSignal){
					session.off();
					session.disconnect();
					window.location = "lsvideo.html";
	        	}else{
					session.disconnect();
					var userBusyMessage = globalizationApp.getLanguageValue("userBusyMessage");
					navigator.notification.alert(
					    userBusyMessage,
					    null,
					    globalizationApp.getLanguageValue("userBusyTitle"),
					    globalizationApp.getLanguageValue("popupButtonDone")
					);
	        	}
            }
        }
    });
    session.connect(data.token, function(){
        console.log("Waiting for:" + selectedName + " to connect. " + selectedUser);
    });
    //update the dialog message
    var dlgMsg = globalizationApp.getLanguageValue("callingUserMessage") + " " + selectedName;
    $(this).find('[data-role="content"] p').html(dlgMsg);
    console.log("IIIIIMMMGGGGG: " + selectedPic);
    $('#picUserCall').attr("src", selectedPic);

	$('#btnCancel').on('click', function() {
		session.off();
		session.disconnect();
		console.log("Cancel call");
	});
});

$(document).on("pagebeforehide", "#CallingDialogPage", function () {
	$('#btnCancel').off();
});


$(document).on("pagecreate","#pagesettings",function(event){
	$('#btnSaveSettings').on('click', function() {
		userManagement.setUserData($("#selectLocLevel option:selected").val(), $("#selectShareControl option:selected").val());
	});

	$("#locationLabel").on("tap",function(){
		var locMessage = globalizationApp.getLanguageValue("popupMsgLocLevel");
		navigator.notification.alert(
		    locMessage,
		    null,
		    globalizationApp.getLanguageValue("popupTitleLocation"),
		    globalizationApp.getLanguageValue("popupButtonDone")
		);
	});

	$("#sharingLabel").on("tap",function(){
		var shaMessage = globalizationApp.getLanguageValue("popupMsgShaLevel");
		navigator.notification.alert(
		    shaMessage,
		    null,
		    globalizationApp.getLanguageValue("popupTitleSharing"),
		    globalizationApp.getLanguageValue("popupButtonDone")
		);
	});

	$('#liSettings').on('click', function() {
		var tutorialMessage1 = globalizationApp.getLanguageValue("tutorial3");
		navigator.notification.alert(tutorialMessage1, null, globalizationApp.getLanguageValue("popupTitleTutorial3"), globalizationApp.getLanguageValue("popupButtonDone"));
	});
});

$(document).on("pageshow", "#pagesettings", function () {
	//START TUTORIAL FOR NEW USER
	if (!(localStorage.getItem("tutorialSettings") === null)) {
		var tutorialMessage3 = globalizationApp.getLanguageValue("tutorial3");
		navigator.notification.alert(tutorialMessage3, null, globalizationApp.getLanguageValue("popupTitleTutorial3"), globalizationApp.getLanguageValue("popupButtonDone"));
    	localStorage.removeItem("tutorialSettings");
	}
});

$(document).on("pagecreate","#pageconfirm",function(event){
	dbManagement.updateConfirmationList();

	$(this).on("click", "#pendingConfirmationList a", function (e) {
		var rowInfo = $(this).closest("li").attr("info");
		var rowLocation = $(this).closest("li").attr("location");
		var rowId = $(this).closest("li").attr("id");
		var rowLat = $(this).closest("li").attr("latitude");
		var rowLon = $(this).closest("li").attr("longitude");
		var rowTs = $(this).closest("li").attr("created");

		window.navigator.notification.confirm(
	        globalizationApp.getLanguageValue("dlgConfirmCardMsg") + rowInfo + ' ' + rowLocation, 
	        function(confirmButton){
	            if (confirmButton == 1){
	                geolocFunctions.share(rowInfo, rowLocation, rowLat, rowLon, rowTs, function(){
                    	dbManagement.moveCard(rowId, "pending_confirm", "shared_cards", function(){
                    		dbManagement.updateLastSharedList();
                    		dbManagement.updateConfirmationList();
                    	});
                    }, function(){
                    	console.log("Error while confirming a CONFIRM sharing");
                    });   
	            } else if (confirmButton == 3){
	            	dbManagement.moveCard(rowId, "pending_confirm", "rejected_cards", function(){
                		dbManagement.updateConfirmationList();
                	});
	            }
	        }, 
	        globalizationApp.getLanguageValue("dlgConfirmCardTitle"), 
	        [globalizationApp.getLanguageValue("dlgConfirmCardYes"),globalizationApp.getLanguageValue("dlgConfirmCardMaybe"),globalizationApp.getLanguageValue("dlgConfirmCardNo")]
	    );
	});

	$('#btnShareAll').on('click', function() {
		window.navigator.notification.confirm(
	        globalizationApp.getLanguageValue("dlgShareAllMsg"), 
	        function(confirmButton){
	            if (confirmButton == 1){
	            	if(navigator.connection.type == Connection.NONE){
			    		console.log("No Internet, send to pending internet");
	                    dbManagement.internetAllPending();
	                }else{
	                	console.log("Share all pending cards");
		                dbManagement.shareAllPending();
	                }
	            }
	        }, 
	        globalizationApp.getLanguageValue("dlgTitleConfirm"), 
	        [globalizationApp.getLanguageValue("dlgShareAllYes"),globalizationApp.getLanguageValue("dlgConfirmCardMaybe")]
	    );
	});

	$('#btnDeleteAll').on('click', function() {
		window.navigator.notification.confirm(
	        globalizationApp.getLanguageValue("dlgDeleteAllMsg"), 
	        function(confirmButton){
	            if (confirmButton == 1){
	                dbManagement.deleteAllPending();
	            }
	        }, 
	        globalizationApp.getLanguageValue("dlgTitleConfirm"), 
	        [globalizationApp.getLanguageValue("dlgDeleteAllYes"),globalizationApp.getLanguageValue("dlgConfirmCardMaybe")]
	    );
	});

	$('#liConfirm').on('click', function() {
		var tutorialMessage1 = globalizationApp.getLanguageValue("tutorial4");
		navigator.notification.alert(tutorialMessage1, null, globalizationApp.getLanguageValue("popupTitleTutorial4"), globalizationApp.getLanguageValue("popupButtonDone"));
	});
});

$(document).on("pageshow", "#pageconfirm", function () {
	window.plugin.notification.badge.set(0);
	localStorage.setItem("confirmBadge", 0);
	$('.countBubl').hide();
	//START TUTORIAL FOR NEW USER
	if (!(localStorage.getItem("tutorialConfirm") === null)) {
		var tutorialMessage4 = globalizationApp.getLanguageValue("tutorial4");
		navigator.notification.alert(tutorialMessage4, null, globalizationApp.getLanguageValue("popupTitleTutorial4"), globalizationApp.getLanguageValue("popupButtonDone"));
    	localStorage.removeItem("tutorialConfirm");
	}
});

function init() {
	var apiRoot = "https://ls-gae-api.appspot.com/_ah/api";
	var apisToLoad;
	var token = localStorage.getItem("tkn");

	var callback = function() {
		if (--apisToLoad == 0) {
			gapi.auth.setToken({
				access_token: token
			});
			window.plugins.insomnia.allowSleepAgain();
			globalizationApp.globalizationInit(function(){
				console.log("Globalization Init Main");
			});
			onStartApp();

			//Action when the app is resumed through clicking a notification
		    window.plugin.notification.local.onclick = onNotificationClick;		    
		    
		    //Action when the app is resumed normally
		    document.addEventListener("resume", onResumeApp, false);
		}
	}

	console.log("INIT MAIN STARTED");
	apisToLoad = 2; // must match number of calls to gapi.client.load()
	gapi.client.load('card', 'v2', callback, apiRoot);
	gapi.client.load('user', 'v2', callback, apiRoot);
}

function onResumeApp(){
	dbManagement.updateLastSharedList();
	dbManagement.updateConfirmationList();
	if((navigator.connection.type != Connection.NONE) && (device.platform != "Android")) {
		dbManagement.shareAllInternetPending();
		dbManagement.processAllGeoPending();
	}
	
	if (!(localStorage.getItem("confirmBadge") === null)) {
        var numberBadge = parseInt(localStorage.getItem("confirmBadge"));
        updateBadges(numberBadge);
    }else{
    	$('.countBubl').hide();
    }
}

function onNotificationClick(id, state, json){
	if (!(localStorage.getItem("confirmBadge") === null)) {
        var numberBadge = parseInt(localStorage.getItem("confirmBadge"));
        localStorage.setItem("confirmBadge", numberBadge-1);
        window.plugin.notification.badge.set(numberBadge-1);
        updateBadges(numberBadge-1);
    }

	if(navigator.connection.type == Connection.NONE){
		console.log("No Internet");
        //Save card in DB for to share when Internet is available
        dbManagement.moveCard(JSON.parse(json).notCardId, "pending_confirm", "pending_internet", dbManagement.updateConfirmationList);
    }else{
    	console.log("Notification captured");
    	if (JSON.parse(json).notInfo != "internet"){
    		geolocFunctions.share(JSON.parse(json).notInfo, JSON.parse(json).notLoc, JSON.parse(json).notLat, JSON.parse(json).notLon, JSON.parse(json).notTs, function(){
            	dbManagement.moveCard(JSON.parse(json).notCardId, "pending_confirm", "shared_cards", function(){
	        		dbManagement.updateLastSharedList();
	        		dbManagement.updateConfirmationList();
	        	});
            }, function(){
            	console.log("Error while confirming a CONFIRM sharing with a notification");
            });
    	}else{
    		console.log("Just update pendings");
    	}
    }
}

function onStartApp(){
	//If it is a new user, it is possible that the sharing settings are set to automatic, 
	//so we need to set the sharing global to TRUE. 
	//This glabal variable indicates the app if the sharing service needs to be up or down.
	if (localStorage.getItem("share") === null) {
        localStorage.setItem("share", "true");
    }

	if (device.platform == "Android"){
    	$('.pageConfirmTab').remove();
    	fillSharedTableAndroid();
    	$('#btnRefreshList').on('click', fillSharedTableAndroid);
    	$('#btnRefreshList').show();
    }

    //We get the user data and configure the app according to it
    userManagement.getUserData();

    //Set the method that will be executed before the user logs out
	$('#btnLogout1, #btnLogout2, #btnLogout3').on('click', userManagement.logout);
		    
	var user_id = localStorage.getItem("user_id");
    //If it is a new user
    //Install and set the Database
    dbManagement.init();
    if (localStorage.getItem(user_id) === null) {
    	//Set variables for the tutorial for new user
    	localStorage.setItem(user_id,1);
    	localStorage.setItem("tutorialMain",1);
    	localStorage.setItem("tutorialFollowers",1);
    	localStorage.setItem("tutorialSettings",1);
    	localStorage.setItem("tutorialConfirm",1);

    	$('.countBubl').hide();
        window.plugin.notification.badge.set(0);

    	$.mobile.changePage($("#pagesettings"), "slide");


		//START TUTORIAL FOR NEW USER
    	$(document).on("pageshow", "#pagemain", function () {
			if (!(localStorage.getItem("tutorialMain") === null)) {
				var tutorialMessage1 = globalizationApp.getLanguageValue("tutorial1");
				navigator.notification.alert(tutorialMessage1, null, globalizationApp.getLanguageValue("popupTitleTutorial1"), globalizationApp.getLanguageValue("popupButtonDone"));
		    	localStorage.removeItem("tutorialMain");
			}
		});
    }else{
    	////Load the last shared cards
    	//dbManagement.updateLastSharedList();
    	//If we are connected, we update the cards that need internet to being processed
    	if((navigator.connection.type != Connection.NONE) && (device.platform != "Android")) {
	    	dbManagement.shareAllInternetPending();
	    	dbManagement.processAllGeoPending();
	    }

	    //Write badges depending on the number of pending cards. Also done on globalization.js due to overwriting
		if (!(localStorage.getItem("confirmBadge") === null)) {
            var numberBadge = parseInt(localStorage.getItem("confirmBadge"));
            updateBadges(numberBadge);
        }else{
        	$('.countBubl').hide();
        	window.plugin.notification.badge.set(0);
        }
    	$.mobile.changePage($("#pagefollowers"), "slide");
    }
}

function updateBadges(numberBadge){
	//$('.pageConfirmTab').find('a').append('<span class="ui-li-count ui-btn-corner-all countBubl">x</span>');
	if (numberBadge > 0){
    	$('.countBubl').show();
    	$('.countBubl').html(numberBadge);
    }else{
    	$('.countBubl').hide();
    	window.plugin.notification.badge.set(0);
    }
}

function fillSharedTableAndroid(){
	$('#lastSharedList').empty();
	gapi.client.card.myNcardsn({'n': 3}).execute(function(resp) {
        if (!resp.code) {
            var f_resp = resp.result;
            var cards = f_resp.cards;
            for (var i = 0; i < cards.length; i++) {
                var card = cards[i];
                console.log("Card: " + card.created + " - " + card.info + " " + card.location);
                var cardDate = new Date(card.created);
				var showDate = cardDate.getDate()
                    + (cardDate.getMonth()+1 < 10 ? "/0" : "/") + (cardDate.getMonth()+1)
                    + (cardDate.getHours() < 10 ? " 0" : " ") + cardDate.getHours()  
                    + (cardDate.getMinutes() < 10 ? ":0" : ":") + cardDate.getMinutes();
                var showLoc = (card.location).split(",");
				$("#lastSharedList").append("<li>" + showDate + " - " + showLoc[0] + "</li>");
            }
        }else{
        	$("#lastSharedList").append("<li>" + globalizationApp.getLanguageValue("sharedCardsTableEmpty") + "</li>");
        }
    });
    $('#lastSharedList').listview('refresh');
}