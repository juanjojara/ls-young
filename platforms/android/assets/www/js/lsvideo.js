$(document).on('deviceready', function() {
    var selectedFId =  localStorage.getItem("followerIdVC");
    var selectedFName =  localStorage.getItem("followerNameVC");
    console.log("Follower: " + selectedFId + " - " + selectedFName);

    $("#picUserCalled").attr("src", localStorage.getItem("followerPicVC"));

    globalizationApp.globalizationInit(function(){
        /*$('#slideEndVideoCall').sliderbutton({
            text: globalizationApp.getLanguageValue("slideToEnd"), // Set slider lane text
            activate: function() { 
              console.log('END CALL!');
              EndCall(sessionVC);
            } // Bind to the activate event during initialization
        });*/
        $('#userCallLabl').html(globalizationApp.getLanguageValue("callingUserMessage") + "...");
        $('#videoCallLabel').html(globalizationApp.getLanguageValue("videoCallFooterLabel") + selectedFName);
    });
    window.plugins.insomnia.keepAwake();

    var xmlhttp = new XMLHttpRequest();
    var sessionURL = "https://opentokrtc.com/" + localStorage.getItem("user_id") + "_" + selectedFId + ".json";
    console.log("Follower URL: " + sessionURL);
    xmlhttp.open("GET", sessionURL, false);
    xmlhttp.send();
    var data = JSON.parse( xmlhttp.response );

    sessionVC = TB.initSession(data.apiKey, data.sid);
    var publisher = TB.initPublisher(data.apiKey,'myPublisherDiv', {width:200, height:150});
    $('#btnEndVideoCall').on('click', function () {
        sessionVC.signal({
            type: "status",
            data: "ciao" + localStorage.getItem("followerIdVC")
        },function(error){
            console.log("SIGNAL CALLBACK");
            if (error) {
              console.log("signal error: " + error.message);
            } else {
              console.log("signal sent");
            }
        });
        publisher.destroy();
        EndCall();
    });
    sessionVC.on({
        'streamCreated': function( event ){
            localStorage.removeItem("waitingVideo");
            $("#waitingAnswer").hide();
            console.log(selectedFName + " connected to the session and STREAM STARTED");
            var subscriber = sessionVC.subscribe( event.stream, 'otherPublisherDiv');
        },
        'streamDestroyed': function( event ){
            console.log("STREAM DESTROYED");
            //EndCall(sessionVC);            
            localStorage.setItem("callEndedUnexpectedly", "true");
            setTimeout(callInterrupted, 10000);
        },
        'sessionDisconnected': function( event ){
            console.log("SESSION FINISHED");
            console.log("The session disconnected. " + event.reason);
            //window.location = "main.html";
            localStorage.setItem("callEndedUnexpectedly", "true");
            setTimeout(callInterrupted, 12000);
        },
        'connectionDestroyed': function( event ){
            console.log("SESSION ENDED IM THE OTHER END POINT");
            //EndCall(sessionVC);
            localStorage.setItem("callEndedUnexpectedly", "true");
            setTimeout(callInterrupted, 14000);
        },
        'signal': function( event ){
            console.log("Signal data: " + event.data);
            if (event.data == "ciao" + localStorage.getItem("user_id")){
                EndCall();
            }
        }
    });
    sessionVC.connect(data.token, function(){
        console.log("Waiting for:" + selectedFName + " to connect. " + selectedFId);
        console.log("Session ID: " + data.sid);
        sessionVC.publish( publisher );
        localStorage.setItem("waitingVideo", "true");
        setTimeout(function(){
            if(!(localStorage.getItem("waitingVideo") === null)){
                localStorage.removeItem("waitingVideo");
                EndCall();
            }
        }, 60000);
    });
});

/*function EndCall(sessionVC){
    console.log("End CALL 1");
    sessionVC.disconnect();
    window.plugins.insomnia.allowSleepAgain();
    console.log("End CALL 2");
}*/

function EndCall(){
    sessionVC.off();
    sessionVC.disconnect();
    localStorage.removeItem("callEndedUnexpectedly");
    localStorage.removeItem("waitingVideo");
    window.plugins.insomnia.allowSleepAgain();
    window.location = "main.html";
}

function callInterrupted(){
    if(!(localStorage.getItem("callEndedUnexpectedly") === null)){
        EndCall();
    }
}