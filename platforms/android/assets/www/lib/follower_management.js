var followerManagement = followerManagement || {};

followerManagement.getMyFollowers = function(){
    gapi.client.user.myfollowers().execute(function(resp) {
        if (!resp.code) {
        	var followers = [];
            var data = resp.result;
            if (!(data.users === undefined)){
				followers = data.users;
				ajax.PopulateListView("call", followers);
			}
        }else{
            $("#error").html(resp.message);
        }
        
    });
}

followerManagement.removeMyFollowers = function(){
    gapi.client.user.myfollowers().execute(function(resp) {
        if (!resp.code) {
            var followers = [];
            var data = resp.result;
            if (!(data.users === undefined)){
                followers = data.users;
                ajax.PopulateListView("delete", followers);
            }
        }else{
            $("#error").html(resp.message);
        }
        
    });
}

followerManagement.getNotMyFollowers = function(){
    gapi.client.user.notmyfollowers().execute(function(resp) {
        if (!resp.code) {
        	var followers = [];
            var data = resp.result;
            if (!(data.users === undefined)){
				followers = data.users;
				ajax.PopulateListView("add", followers);
			}
        }else{
            $("#error").html(resp.message);
        }
        
    });
}

followerManagement.addFollower = function(userId){
    gapi.client.user.followmeuserId({"userId":userId}).execute(function(resp) {
        if (!resp.code) {
        	console.log("add YES " + userId);
        	var itemId = "#" + userId;
        	$(itemId).remove();
            //var itemId = document.getElementById("#" + userId);
            //$('#usersList').removeChild(itemId);
        	
            followerManagement.getMyFollowers();
            followerManagement.removeMyFollowers();
        }else{
        	console.log("add NO");
            $("#error").html(resp.message);
        }
        
    });
}

followerManagement.removeFollower = function(userId){
    gapi.client.user.remove_me_fromuserId({"userId":userId}).execute(function(resp) {
        console.log(resp);
        if (!resp.code) {
        	console.log("remove YES" + userId);
        	var itemId = "#" + userId;
        	$(itemId).remove();
            $(itemId).remove();
        	followerManagement.getNotMyFollowers();
            //followerManagement.getMyFollowers();
        }else{
        	console.log("remove NO");
            $("#error").html(resp.message);
        }
        
    });
}

var ajax = {  
    PopulateListView:function(action, followers){
    	followers.sort(compare);
    	if (action == "delete"){
    		$('#followersDeleteList').empty();
        	$.each(followers, function(i, row) {
                $('#followersDeleteList').append('<li data-icon="minus" id="' + row.id + '" user-name="' + row.username + '" user-pic="' + row.picture + '"><a href="#YesNoDialogPage" style="white-space:normal;" data-id="' + row.id + '"><img src="' + row.picture + '">' + row.username + '</a></li>');
        	});
        	$('#followersDeleteList').listview('refresh');
    	}else if (action == "add"){
    		$('#usersList').empty();
    		$.each(followers, function(i, row) {
    			$('#usersList').append('<li data-icon="plus" id="' + row.id + '" user-name="' + row.username + '" user-pic="' + row.picture + '"><a href="#YesNoDialogPage" style="white-space:normal;" data-id="' + row.id + '"><img src="' + row.picture + '">' + row.username + '</a></li>');
    		});
    		$('#usersList').listview('refresh');
    	}else {
            $('#followersList').empty();
            if (device.platform == "Android"){
                $.each(followers, function(i, row) {
                    $('#followersList').append('<li data-icon="phone" id="' + row.id + '" user-name="' + row.username + '" user-pic="' + row.picture + '"><a href="#" style="white-space:normal;" data-id="' + row.id + '"><img src="' + row.picture + '">' + row.username + '</a></li>');
                });
            } else {
                $.each(followers, function(i, row) {
                    $('#followersList').append('<li data-icon="phone" id="' + row.id + '" user-name="' + row.username + '" user-pic="' + row.picture + '"><a href="#" style="white-space:normal;" data-id="' + row.id + '"><img src="' + row.picture + '">' + row.username + '</a></li>');
                    //$('#followersList').append('<li data-icon="phone" id="' + row.id + '" user-name="' + row.username + '" user-pic="' + row.picture + '"><a href="#CallingDialogPage" style="white-space:normal;" data-id="' + row.id + '"><img src="' + row.picture + '">' + row.username + '</a></li>');
                });
            }
            $('#followersList').listview('refresh');
        }
    }
}

function compare(a,b) {
  if (a.username.toUpperCase() < b.username.toUpperCase())
     return -1;
  if (a.username.toUpperCase() > b.username.toUpperCase())
    return 1;
  return 0;
}
