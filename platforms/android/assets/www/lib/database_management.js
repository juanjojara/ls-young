var dbManagement = dbManagement || {};

var dbInstance;

dbManagement.init = function(){
    var db = dbUtil.getDBInstance();
    db.transaction(populateDB, function(err){
        console.log("Error while initializing DB. Error: " + err.message + ". Code="+err.code);
	});

	function populateDB(tx) {
		/*tx.executeSql('DROP TABLE IF EXISTS pending_geo');
		tx.executeSql('DROP TABLE IF EXISTS pending_confirm');
		tx.executeSql('DROP TABLE IF EXISTS pending_internet');
		tx.executeSql('DROP TABLE IF EXISTS shared_cards');
		tx.executeSql('DROP TABLE IF EXISTS rejected_cards');*/
		tx.executeSql('CREATE TABLE IF NOT EXISTS pending_geo (id INTEGER PRIMARY KEY, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm)');
		tx.executeSql('CREATE TABLE IF NOT EXISTS pending_confirm (id INTEGER PRIMARY KEY, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm)');
		tx.executeSql('CREATE TABLE IF NOT EXISTS pending_internet (id INTEGER PRIMARY KEY, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm)');
		tx.executeSql('CREATE TABLE IF NOT EXISTS shared_cards (id INTEGER PRIMARY KEY, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm)', [], dbManagement.updateLastSharedList);
		tx.executeSql('CREATE TABLE IF NOT EXISTS rejected_cards (id INTEGER PRIMARY KEY, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm)');
		//tx.executeSql('CREATE TABLE IF NOT EXISTS bridge (id INTEGER)');
	}

	function getAllCards(tx) {
		var user_id = localStorage.getItem("user_id");
		tx.executeSql("SELECT id, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm FROM pending_geo WHERE user_id = ? ORDER BY id DESC LIMIT 5",[user_id],renderEntries);
		tx.executeSql("SELECT id, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm FROM pending_confirm WHERE user_id = ? ORDER BY id DESC LIMIT 5",[user_id],renderEntries);
		tx.executeSql("SELECT id, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm FROM pending_internet WHERE user_id = ? ORDER BY id DESC LIMIT 5",[user_id],renderEntries);
		tx.executeSql("SELECT id, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm FROM shared_cards WHERE user_id = ? ORDER BY id DESC LIMIT 5",[user_id],renderEntries);
		tx.executeSql("SELECT id, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm FROM rejected_cards WHERE user_id = ? ORDER BY id DESC LIMIT 5",[user_id],renderEntries);
	}

	function renderEntries(tx,results){
		console.log("NEW TABLE");
		if (results.rows.length == 0) {
			console.log("No cards");
		} else {
			for(var i=0; i<results.rows.length; i++) {
				console.log(results.rows.item(i).id + " - " + results.rows.item(i).created + " - " + results.rows.item(i).location + " - " + results.rows.item(i).sharing_level);
			}
		}
	}
}

dbManagement.deleteAllPending = function(){
	var user_id = localStorage.getItem("user_id");
	var db = dbUtil.getDBInstance();
    db.transaction(rejectAllCards);

    function rejectAllCards(tx){
    	tx.executeSql("INSERT INTO rejected_cards (id, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm) SELECT id, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm FROM pending_confirm WHERE user_id = ?",[user_id], function(){
    		dbManagement.deletePendingCards(user_id);
    	}, dbUtil.dbErrorHandler);
	}
}

dbManagement.internetAllPending = function(){
	var user_id = localStorage.getItem("user_id");
	var db = dbUtil.getDBInstance();
    db.transaction(internetAllCards);

    function internetAllCards(tx){
    	tx.executeSql("INSERT INTO pending_internet (id, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm) SELECT id, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm FROM pending_confirm WHERE user_id = ?",[user_id], function(){
    		dbManagement.deletePendingCards(user_id);
    	}, dbUtil.dbErrorHandler);
	}
}

dbManagement.shareAllPending = function(){
	var user_id = localStorage.getItem("user_id");
	var db = dbUtil.getDBInstance();
    db.transaction(getPendingConfirmCards);

    function getPendingConfirmCards(tx) {
		tx.executeSql("SELECT id, created, info, location, latitude, longitude FROM pending_confirm WHERE user_id = ? ORDER BY id",[user_id],shareAllCards, dbUtil.dbErrorHandler);
	}

    function shareAllCards(tx,results){
    	if (results.rows.length == 0) {
			Console.log("No pending cards to share");
		} else {
			for(var i=0; i<results.rows.length; i++) {
				var cardId = results.rows.item(i).id;
				var cardTs = results.rows.item(i).created;
				var cardInfo = results.rows.item(i).info;
				var cardLoc = results.rows.item(i).location;
				var cardLat = results.rows.item(i).latitude;
				var cardLon = results.rows.item(i).longitude;

				geolocFunctions.share(cardInfo, cardLoc, cardLat, cardLon, cardTs, function(){
		        	dbManagement.moveCard(cardId, "pending_confirm", "shared_cards", function(){
		        		dbManagement.updateLastSharedList();
		        		dbManagement.updateConfirmationList();
		        	});
		        }, function(){
                    	console.log("Error while confirming ALL CONFIRM sharing");
                });
			}
		}
	}
}

dbManagement.shareAllInternetPending = function(){
	var db = dbUtil.getDBInstance();
    db.transaction(getPendingInternetCards, dbUtil.dbErrorHandler2);

    function getPendingInternetCards(tx) {
    	var user_id = localStorage.getItem("user_id");
		tx.executeSql("SELECT id, created, info, location, latitude, longitude FROM pending_internet WHERE user_id = ? ORDER BY id DESC",[user_id],shareInternetCards, dbUtil.dbErrorHandler);
	}

    function shareInternetCards(tx,results){
    	if (results.rows.length == 0) {
			console.log("No internet pending cards to share");
		} else {
			for(var i=0; i<results.rows.length; i++) {
				var cardId = results.rows.item(i).id;
				var cardTs = results.rows.item(i).created;
				var cardInfo = results.rows.item(i).info;
				var cardLoc = results.rows.item(i).location;
				var cardLat = results.rows.item(i).latitude;
				var cardLon = results.rows.item(i).longitude;

				geolocFunctions.share(cardInfo, cardLoc, cardLat, cardLon, cardTs, function(){
		        	dbManagement.moveCard(cardId, "pending_internet", "shared_cards", dbManagement.updateLastSharedList);
		        }, function(){
                    	console.log("Error while sharing an Internet pending Card");
                });
			}
		}
	}
}

dbManagement.processAllGeoPending = function(){
	var user_id = localStorage.getItem("user_id");
	var db = dbUtil.getDBInstance();
    db.transaction(getPendingGeoCards, dbUtil.dbErrorHandler2);

    function getPendingGeoCards(tx) {
		tx.executeSql("SELECT id, created, latitude, longitude, sharing_level, location_level, confirm FROM pending_geo WHERE user_id = ? ORDER BY id LIMIT 10",[user_id],processGeoCards, dbUtil.dbErrorHandler);
	}

    function processGeoCards(tx,results){
    	if (results.rows.length == 0) {
			console.log("No geo pending cards to share");
		} else {
			for(var i=0; i<results.rows.length; i++) {
				var cardId = results.rows.item(i).id;
				var cardTs = results.rows.item(i).created;
				var cardLat = results.rows.item(i).latitude;
				var cardLon = results.rows.item(i).longitude;
				var cardShareSet = results.rows.item(i).sharing_level;
				var cardLocSet = results.rows.item(i).location_level;
				var cardConfirm = results.rows.item(i).confirm;

				geolocFunctions.reverseGeolocationRecovery(cardLat, cardLon, cardTs, cardLocSet, function(recInfo, recLoc){
					var dbCB = dbUtil.getDBInstance();
    				dbCB.transaction(function(txCB){
						txCB.executeSql("UPDATE pending_geo SET info = ?, location = ? WHERE id = ?",[recInfo, recLoc, cardId], function(){
							if (cardConfirm == true){
								console.log("CONFIRM GEO PENDING: " + recInfo + " - " + recLoc + " - " + cardLat + " - " + cardLon);
								dbManagement.moveCard(cardId, "pending_geo", "pending_confirm", dbManagement.updateConfirmationList);
							} else {
								console.log("SHARE GEO PENDING: " + recInfo + " - " + recLoc + " - " + cardLat + " - " + cardLon);
								geolocFunctions.share(recInfo, recLoc, cardLat, cardLon, cardTs, function(){
						        	dbManagement.moveCard(cardId, "pending_geo", "shared_cards", dbManagement.updateLastSharedList);
						        }, function(){
			                    	console.log("Error while sharing an automatic GEO pending card");
			                    });
							}
						}, dbUtil.dbErrorHandler);
    				});
		        }, function(){
		        	var dbCB = dbUtil.getDBInstance();
    				dbCB.transaction(function(txCB){
			        	txCB.executeSql("DELETE FROM pending_geo WHERE id = ?",[cardId], dbUtil.dbSuccess, dbUtil.dbErrorHandler);
    				}, dbUtil.dbErrorHandler);
		        });
			}
		}
	}
}

dbManagement.moveCard = function(cardId, fromTable, toTable, txCallback){
	var db = dbUtil.getDBInstance();
    db.transaction(moveCardFromTo, dbUtil.dbErrorHandler);

    function moveCardFromTo(tx){
    	var queryInsert = "INSERT INTO " + toTable + " (id, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm) SELECT id, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm FROM " + fromTable + " WHERE id = ?";
    	var queryDelete = "DELETE FROM " + fromTable + " WHERE id = ?";
    	if (txCallback === null){
            console.log("Transaction callback is null");
            txCallback = dbUtil.dbSuccess;
        }
    	tx.executeSql(queryInsert,[cardId], function(){
    		tx.executeSql(queryDelete,[cardId], txCallback, dbUtil.dbErrorHandler);
    	}, dbUtil.dbErrorHandler);
	}
}

dbManagement.updateLastSharedList = function(){
	var user_id = localStorage.getItem("user_id");
	var db = dbUtil.getDBInstance();

	if (device.platform != "Android"){
	    db.transaction(getSharedCards, function(err){
	        console.log("Error while updating list of last shared cards. Error: " + err.message + ". Code="+err.code);
		});
	}else{
		console.log("Cards shared. Update shared list manually")
	}

    //I handle getting entries from the db
	function getSharedCards(tx) {
		tx.executeSql("SELECT id, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm FROM shared_cards WHERE user_id = ? ORDER BY id DESC LIMIT 3",[user_id],fillSharedTable, function(err){
			console.log("Error while getting the last the shared cards. Error: " + err.message + ". Code="+err.code);
		});
	}

	function fillSharedTable(tx,results){
		$('#lastSharedList').empty();
		if (results.rows.length == 0) {
			$("#lastSharedList").append("<li>" + globalizationApp.getLanguageValue("sharedCardsTableEmpty") + "</li>");
		} else {
			for(var i=0; i<results.rows.length; i++) {
				var cardDate = new Date(results.rows.item(i).created);
				var showDate = cardDate.getDate()
                    + (cardDate.getMonth()+1 < 10 ? "/0" : "/") + (cardDate.getMonth()+1)
                    + (cardDate.getHours() < 10 ? " 0" : " ") + cardDate.getHours()  
                    + (cardDate.getMinutes() < 10 ? ":0" : ":") + cardDate.getMinutes();
                var showLoc = (results.rows.item(i).location).split(",");
				$("#lastSharedList").append("<li>" + showDate + " - " + showLoc[0] + "</li>");
			}
		}
		$('#lastSharedList').listview('refresh');
	}
}

dbManagement.updateConfirmationList = function(){
	var user_id = localStorage.getItem("user_id");
	var db = dbUtil.getDBInstance();

	if (device.platform != "Android"){
	    db.transaction(getPendingConfirmCards, function(err){
	        console.log("Error while updating list of cards that need confirmation. Error: " + err.message + ". Code="+err.code);
		});
	}

    //I handle getting entries from the db
	function getPendingConfirmCards(tx) {
		tx.executeSql("SELECT id, created, info, location, latitude, longitude, sharing_level, location_level, user_id, confirm FROM pending_confirm WHERE user_id = ? ORDER BY id DESC LIMIT 10",[user_id],fillConfirmTable, dbUtil.dbErrorHandler);
	}

	function fillConfirmTable(tx,results){
		$('#pendingConfirmationList').empty();
		if (results.rows.length == 0) {
			$("#pendingConfirmationList").append("<li>" + globalizationApp.getLanguageValue("pendingCardsTableEmpty") + "</li>");
		} else {
			for(var i=0; i<results.rows.length; i++) {
				var cardDate = new Date(results.rows.item(i).created);
				var showDate = cardDate.getDate()
                    + (cardDate.getMonth()+1 < 10 ? "/0" : "/") + (cardDate.getMonth()+1)
                    + (cardDate.getHours() < 10 ? " 0" : " ") + cardDate.getHours()  
                    + (cardDate.getMinutes() < 10 ? ":0" : ":") + cardDate.getMinutes();
                var showLoc = (results.rows.item(i).location).split(",");
				$("#pendingConfirmationList").append('<li data-icon="check" id="' + results.rows.item(i).id + '" info="' + results.rows.item(i).info + '" location="' + results.rows.item(i).location + '" latitude="' + results.rows.item(i).latitude + '" longitude="' + results.rows.item(i).longitude + '" created="' + results.rows.item(i).created + '"><a href="#">' + showDate + ' - ' + showLoc[0] + '</a></li>');
			}
		}
		$('#pendingConfirmationList').listview('refresh');
	}
}

dbManagement.deletePendingCards = function(userId){
	var db = dbUtil.getDBInstance();
    db.transaction(rejectCardsUser);

    function rejectCardsUser(tx){
		tx.executeSql("DELETE FROM pending_confirm WHERE user_id = ?",[userId], dbManagement.updateConfirmationList, dbUtil.dbErrorHandler);
	}
}

var dbUtil = {
	getDBInstance: function(){
		var retInstance;
		if (!(dbInstance === undefined)) {
	    	retInstance = dbInstance;
	    }else{
	    	if (device.platform == "Android"){
	    		retInstance = window.openDatabase("dbLifeshare", "1", "Lifeshare Cards", 1*1024*512);
	    	}else{
	    		retInstance = window.openDatabase("dbLifeshare", "1.0", "Lifeshare Cards", 1*1024*1024);	    		
	    	}
	    	//retInstance = window.sqlitePlugin.openDatabase({name: "dbLifeshare.db"});
	    	dbInstance = retInstance;
	    }
	    return retInstance;
	},

	dbErrorHandler: function(err){
        console.log("DB Error: " + err.message + ". Code="+err.code);
	},

	dbErrorHandler2: function(err){
		console.log("DEBUG Transaction");
        console.log("DB Error: " + err.message + ". Code="+err.code);
	},

	dbSuccess: function(){
        console.log("Insert successful");
    }
}