var userManagement = userManagement || {};

userManagement.getUserData = function(){
    console.log("Settings START");
    gapi.client.user.me().execute(function(resp) {
        if (!resp.code) {
            $('#lblSettingsFooter, #lblFollowersFooter, #lblMainFooter').html('<font size="2">' + resp.username + '</font>');
            $('#picSettingsFooter, #picFollowersFooter, #picMainFooter').attr("src", resp.picture);
            $('#selectLocLevel').val(resp.location_level).change();
            $('#selectShareControl').val(resp.sharing_level).change();
            console.log("USER LOAD OK!!!");
            localStorage.setItem("user_photo", resp.picture);
            localStorage.setItem("location_setting", resp.location_level);
            localStorage.setItem("sharing_setting", resp.sharing_level);
            localStorage.setItem("user_name", resp.username);
            
            if (resp.sharing_level == "button"){
                userManagement.enableButtonSharing();
            }else{
                userManagement.enableBackgroundSharing();
            } 
        }else{
            console.log("Settings ERROR LOAD");
            $("#error").html(resp.message);
        }
    });
}

userManagement.setUserData = function(loc_level, share_level){
    console.log("Settings " + loc_level + " - " + share_level);
    gapi.client.user.set_privacy({"location_level": loc_level, "sharing_level": share_level}).execute(function(resp) {
        if (!resp.code) {
            console.log("Settings saved OK");

            if ((localStorage.getItem("sharing_setting") == "button") && (share_level != "button")) {
                localStorage.setItem("share", "true");
                userManagement.enableBackgroundSharing();
            } else if ((localStorage.getItem("sharing_setting") != "button") && (share_level == "button")) {
                userManagement.enableButtonSharing();
            }

            localStorage.setItem("location_setting", loc_level);
            localStorage.setItem("sharing_setting", share_level);

            $("#savedSettingsPopup p").html(globalizationApp.getLanguageValue("popupMsgSettingsSaved"));
            setTimeout(function () {
                $('#savedSettingsPopup').popup('close');
             }, 2000);
        }else{
            $("#savedSettingsPopup p").html(globalizationApp.getLanguageValue("popupMsgSettingsNotSaved"));
            setTimeout(function () {
                $('#savedSettingsPopup').popup('close');
             }, 2000);
            $("#error").html(resp.message);
        }
    });
}

userManagement.enableButtonSharing = function(){
    $('#btnSharing').hide();
    $('#shakeLabels').show();

    if (window.plugins.backgroundGeoLocation) {
        console.log("STOPPING BACKGROUND SERVICE");
        app.stopBackgroundGeoLocation();
        console.log("BACKGROUND SERVICE STOPPED");
    }

    // Start watching for shake gestures and call "onShake"
    shake.startWatch(function(){
        console.log("SHAKE SHARING!!");
        userManagement.shareLocApp();
    });
}

userManagement.enableBackgroundSharing = function(){
    $('#btnSharing').show();
    $('#shakeLabels').hide();

    // Stop watching for shake gestures
    shake.stopWatch();

    console.log("AUTOMATIC SHARING:" + localStorage.getItem("share"));
    if ((window.plugins.backgroundGeoLocation) && (localStorage.getItem("share") == "true")) {
        console.log("INITIALIZING BACKGROUND SERVICE");
        app.configureBackgroundGeoLocation();
        console.log("BACKGROUND SERVICE INITIALIZED");
    }
}

userManagement.logout = function(){
    localStorage.removeItem("tkn");
    localStorage.removeItem("user_id");

    // Stop watching for shake gestures
    shake.stopWatch();

    //Stop the background service
    if (window.plugins.backgroundGeoLocation) {
        console.log("STOPPING BACKGROUND SERVICE");
        app.stopBackgroundGeoLocation();
        console.log("BACKGROUND SERVICE STOPPED");
    }

    window.location="index.html";
}

userManagement.shareLocApp = function(){
    app.shareLocation();
}
